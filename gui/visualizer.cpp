#include "visualizer.h"

#include <filesystem>
#include <fstream>
#include <iomanip>

#include <lib.h>
#include <cuMat/src/Errors.h>
#include <GL/glew.h>
#include <cuda_gl_interop.h>
#include <cuMat/src/Context.h>

#define NOMINMAX
#include <windows.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "imgui/imgui.h"
#include "imgui/IconsFontAwesome5.h"
#include "imgui/imgui_extension.h"
#include "imgui/imgui_internal.h"

#include <json.hpp>
#include <lodepng.h>
#include <portable-file-dialogs.h>
#include "utils.h"

const char* Visualizer::RedrawModeNames[] = {
	"None", "Post", "Renderer"
};
const char* Visualizer::ChannelModeNames[] = {
	"Mask", "Normal", "Depth", "AO", "Flow", "Color"
};

Visualizer::Visualizer(GLFWwindow* window)
	: window_(window)
{
	// Add .ini handle for ImGuiWindow type
	ImGuiSettingsHandler ini_handler;
	ini_handler.TypeName = "Visualizer";
	ini_handler.TypeHash = ImHashStr("Visualizer");
	static const auto replaceWhitespace = [](const std::string& s) -> std::string
	{
		std::string cpy = s;
		for (int i = 0; i < cpy.size(); ++i)
			if (cpy[i] == ' ') cpy[i] = '%'; //'%' is not allowed in path names
		return cpy;
	};
	static const auto insertWhitespace = [](const std::string& s) -> std::string
	{
		std::string cpy = s;
		for (int i = 0; i < cpy.size(); ++i)
			if (cpy[i] == '%') cpy[i] = ' '; //'%' is not allowed in path names
		return cpy;
	};
	auto settingsReadOpen = [](ImGuiContext*, ImGuiSettingsHandler* handler, const char* name) -> void*
	{
		return handler->UserData;
	};
	auto settingsReadLine = [](ImGuiContext*, ImGuiSettingsHandler* handler, void* entry, const char* line)
	{
		Visualizer* vis = reinterpret_cast<Visualizer*>(handler->UserData);
		char path[MAX_PATH];
		int intValue = 0;
		memset(path, 0, sizeof(char)*MAX_PATH);
		std::cout << "reading \"" << line << "\"" << std::endl;
		if (sscanf(line, "VolumeDir=%s", path) == 1)
			vis->volumeDirectory_ = insertWhitespace(std::string(path));
		if (sscanf(line, "TfDir=%s", path) == 1)
			vis->tfDirectory_ = insertWhitespace(std::string(path));
		if (sscanf(line, "SettingsDir=%s", path) == 1)
			vis->settingsDirectory_ = insertWhitespace(std::string(path));
		if (sscanf(line, "SettingsToLoad=%d", &intValue) == 1)
			vis->settingsToLoad_ = intValue;
	};
	auto settingsWriteAll = [](ImGuiContext* ctx, ImGuiSettingsHandler* handler, ImGuiTextBuffer* buf)
	{
		Visualizer* vis = reinterpret_cast<Visualizer*>(handler->UserData);
		buf->reserve(200);
		buf->appendf("[%s][Settings]\n", handler->TypeName);
		std::string volumeDir = replaceWhitespace(vis->volumeDirectory_);
		std::string tfDir = replaceWhitespace(vis->tfDirectory_);
		std::string settingsDirectory = replaceWhitespace(vis->settingsDirectory_);
		std::cout << "Write settings:" << std::endl;
		buf->appendf("VolumeDir=%s\n", volumeDir.c_str());
		buf->appendf("TfDir=%s\n", tfDir.c_str());
		buf->appendf("SettingsDir=%s\n", settingsDirectory.c_str());
		buf->appendf("SettingsToLoad=%d\n", vis->settingsToLoad_);
		buf->appendf("\n");
	};
	ini_handler.UserData = this;
	ini_handler.ReadOpenFn = settingsReadOpen;
	ini_handler.ReadLineFn = settingsReadLine;
	ini_handler.WriteAllFn = settingsWriteAll;
	GImGui->SettingsHandlers.push_back(ini_handler);

	//initialize renderer
	renderer::initializeRenderer();

	//initialize test volume
	volume_ = renderer::Volume::createSphere(16);
	volume_->getLevel(0)->copyCpuToGpu();
	volumeMipmapLevel_ = 0;
	volumeFilename_ = "Initial";
	volumeHistogram_ = volume_->extractHistogram();
	minDensity_ = (minDensity_ < volumeHistogram_.maxDensity && minDensity_ > volumeHistogram_.minDensity) ? minDensity_ : volumeHistogram_.minDensity;
	maxDensity_ = (maxDensity_ < volumeHistogram_.maxDensity && maxDensity_ > volumeHistogram_.minDensity) ? maxDensity_ : volumeHistogram_.maxDensity;
}

Visualizer::~Visualizer()
{
	releaseResources();
}

void Visualizer::releaseResources()
{
	if (screenTextureCuda_)
	{
		CUMAT_SAFE_CALL(cudaGraphicsUnregisterResource(screenTextureCuda_));
		screenTextureCuda_ = nullptr;
	}
	if (screenTextureGL_)
	{
		glDeleteTextures(1, &screenTextureGL_);
		screenTextureGL_ = 0;
	}
	if (screenTextureCudaBuffer_)
	{
		CUMAT_SAFE_CALL(cudaFree(screenTextureCudaBuffer_));
		screenTextureCudaBuffer_ = nullptr;
	}
	if (postOutput_)
	{
		CUMAT_SAFE_CALL(cudaFree(postOutput_));
		postOutput_ = nullptr;
	}
}

void Visualizer::settingsSave()
{
	// save file dialog
	auto fileNameStr = pfd::save_file(
		"Save settings",
		settingsDirectory_,
		{ "Json file", "*.json" },
		true
	).result();
	if (fileNameStr.empty())
		return;

	auto fileNamePath = std::filesystem::path(fileNameStr);
	fileNamePath = fileNamePath.replace_extension(".json");
	std::cout << "Save settings to " << fileNamePath << std::endl;
	settingsDirectory_ = fileNamePath.string();

	// Build json
	nlohmann::json settings;
	settings["version"] = 1;
	//camera
	settings["camera"] = cameraGui_.toJson();
	//computation mode
	settings["renderMode"] = renderMode_;
	//TF editor
	settings["tfEditor"] = {
		{"editor", editor_.toJson()},
		{"minDensity", minDensity_},
		{"maxDensity", maxDensity_},
		{"opacityScaling", opacityScaling_},
		{"showColorControlPoints", showColorControlPoints_},
		{"dvrUseShading", dvrUseShading_}
	};
	//render parameters
	settings["renderer"] = {
		{"isovalue", rendererArgs_.isovalue},
		{"stepsize", rendererArgs_.stepsize},
		{"filterMode", rendererArgs_.volumeFilterMode},
		{"binarySearchSteps", rendererArgs_.binarySearchSteps},
		{"aoSamples", rendererArgs_.aoSamples},
		{"aoRadius", rendererArgs_.aoRadius}
	};
	//shading
	settings["shading"] = {
		{"materialColor", materialColor},
		{"ambientLight", ambientLightColor},
		{"diffuseLight", diffuseLightColor},
		{"specularLight", specularLightColor},
		{"specularExponent", specularExponent},
		{"aoStrength", aoStrength},
		{"lightDirection", lightDirectionScreen},
		{"channel", channelMode_},
		{"flowWithInpainting", flowWithInpainting_},
		{"temporalSmoothing", temporalPostSmoothingPercentage_}
	};
	//don't save volumes or networks as they depend on external files

	//save json to file
	std::ofstream o(fileNamePath);
	o << std::setw(4) << settings << std::endl;
	screenshotString_ = std::string("Settings saved to ") + fileNamePath.string();
	screenshotTimer_ = 2.0f;
}

namespace
{
	std::string getDir(const std::string& path)
	{
		if (path.empty())
			return path;
		std::filesystem::path p(path);
		if (std::filesystem::is_directory(p))
			return path;
		return p.parent_path().string();
	}
}

void Visualizer::settingsLoad()
{
	// load file dialog
	auto results = pfd::open_file(
        "Load settings",
        getDir(settingsDirectory_),
        { "Json file", "*.json" },
        false
    ).result();
	if (results.empty())
		return;

	auto fileNameStr = results[0];
	auto fileNamePath = std::filesystem::path(fileNameStr);
	std::cout << "Load settings from " << fileNamePath << std::endl;
	settingsDirectory_ = fileNamePath.string();

	//load json
	std::ifstream i(fileNamePath);
	nlohmann::json settings;
	try
	{
		i >> settings;
	} catch (const nlohmann::json::exception& ex)
	{
		pfd::message("Unable to parse Json", std::string(ex.what()),
			pfd::choice::ok, pfd::icon::error).result();
		return;
	}
	i.close();
	int version = settings.contains("version")
		? settings.at("version").get<int>()
		: 0;
	if (version != 1)
	{
		pfd::message("Illegal Json", "The loaded json does not contain settings in the correct format",
			pfd::choice::ok, pfd::icon::error).result();
		return;
	}

	//Ask which part should be loaded
	static bool loadCamera, loadComputationMode, loadTFeditor, loadRenderer, loadShading;
	static bool popupOpened;
	loadCamera = settingsToLoad_ & CAMERA;
	loadComputationMode = settingsToLoad_ & COMPUTATION_MODE;
	loadTFeditor = settingsToLoad_ & TF_EDITOR;
	loadRenderer = settingsToLoad_ & RENDERER;
	loadShading = settingsToLoad_ & SHADING;
	popupOpened = false;
	auto guiTask = [this, settings]()
	{
		if (!popupOpened)
		{
			ImGui::OpenPopup("What to load");
			popupOpened = true;
			std::cout << "Open popup" << std::endl;
		}
		if (ImGui::BeginPopupModal("What to load", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Checkbox("Camera##LoadSettings", &loadCamera);
			ImGui::Checkbox("Computation Mode##LoadSettings", &loadComputationMode);
			ImGui::Checkbox("TF Editor##LoadSettings", &loadTFeditor);
			ImGui::Checkbox("Renderer##LoadSettings", &loadRenderer);
			ImGui::Checkbox("Shading##LoadSettings", &loadShading);
			if (ImGui::Button("Load##LoadSettings") || ImGui::IsKeyPressedMap(ImGuiKey_Enter))
			{
				try
				{
					//apply new settings
					if (loadCamera)
					{
						cameraGui_.fromJson(settings.at("camera"));
					}
					if (loadComputationMode)
					{
						renderMode_ = settings.at("renderMode").get<RenderMode>();
					}
					if (loadTFeditor)
					{
						const auto& s = settings.at("tfEditor");
						editor_.fromJson(s.at("editor"));
						minDensity_ = s.at("minDensity").get<float>();
						maxDensity_ = s.at("maxDensity").get<float>();
						opacityScaling_ = s.at("opacityScaling").get<float>();
						showColorControlPoints_ = s.at("showColorControlPoints").get<bool>();
						dvrUseShading_ = s.at("dvrUseShading").get<bool>();
					}
					if (loadRenderer)
					{
						const auto& s = settings.at("renderer");
						rendererArgs_.isovalue = s.at("isovalue").get<double>();
						rendererArgs_.stepsize = s.at("stepsize").get<double>();
						rendererArgs_.volumeFilterMode = s.at("filterMode").get<renderer::RendererArgs::VolumeFilterMode>();
						rendererArgs_.binarySearchSteps = s.at("binarySearchSteps").get<int>();
						rendererArgs_.aoSamples = s.at("aoSamples").get<int>();
						rendererArgs_.aoRadius = s.at("aoRadius").get<double>();
					}
					if (loadShading)
					{
						const auto& s = settings.at("shading");
						materialColor = s.at("materialColor").get<float3>();
						ambientLightColor = s.at("ambientLight").get<float3>();
						diffuseLightColor = s.at("diffuseLight").get<float3>();
						specularLightColor = s.at("specularLight").get<float3>();
						specularExponent = s.at("specularExponent").get<float>();
						aoStrength = s.at("aoStrength").get<float>();
						lightDirectionScreen = s.at("lightDirection").get<float3>();
						channelMode_ = s.at("channel").get<ChannelMode>();
						flowWithInpainting_ = s.at("flowWithInpainting").get<bool>();
						temporalPostSmoothingPercentage_ = s.at("temporalSmoothing").get<int>();
					}
					//save last selection
					settingsToLoad_ =
						(loadCamera ? CAMERA : 0) |
						(loadComputationMode ? COMPUTATION_MODE : 0) |
						(loadTFeditor ? TF_EDITOR : 0) |
						(loadRenderer ? RENDERER : 0) |
						(loadShading ? SHADING : 0);
					ImGui::MarkIniSettingsDirty();
					ImGui::SaveIniSettingsToDisk(GImGui->IO.IniFilename);
					std::cout << "Settings applied" << std::endl;
				} catch (const nlohmann::json::exception& ex)
				{
					std::cerr << "Error: id=" << ex.id << ", message: " << ex.what() << std::endl;
					pfd::message("Unable to apply settings",
						std::string(ex.what()),
						pfd::choice::ok, pfd::icon::error).result();
				}
				//close popup
				this->backgroundGui_ = {};
				ImGui::CloseCurrentPopup();
				triggerRedraw(RedrawRenderer);
			}
			ImGui::SameLine();
			if (ImGui::Button("Cancel##LoadSettings") || ImGui::IsKeyPressedMap(ImGuiKey_Escape))
			{
				//close popup
				this->backgroundGui_ = {};
				ImGui::CloseCurrentPopup();
				triggerRedraw(RedrawRenderer);
			}
			ImGui::EndPopup();
		}
	};
	worker_.wait(); //wait for current task
	this->backgroundGui_ = guiTask;
}

void Visualizer::loadVolume()
{
	std::cout << "Open file dialog" << std::endl;

	// open file dialog
	auto results = pfd::open_file(
        "Load volume",
        getDir(volumeDirectory_),
        { "Volumes", "*.dat *.xyz *.cvol" },
        false
    ).result();
	if (results.empty())
		return;
	std::string fileNameStr = results[0];

	std::cout << "Load " << fileNameStr << std::endl;
	auto fileNamePath = std::filesystem::path(fileNameStr);
	volumeDirectory_ = fileNamePath.string();
	ImGui::MarkIniSettingsDirty();
	ImGui::SaveIniSettingsToDisk(GImGui->IO.IniFilename);

	//load the file
	worker_.wait(); //wait for current task
	std::shared_ptr<float> progress = std::make_shared<float>(0);
	auto guiTask = [progress]()
	{
		std::cout << "Progress " << *progress.get() << std::endl;
		if (ImGui::BeginPopupModal("Load Volume", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::ProgressBar(*progress.get(), ImVec2(200, 0));
			ImGui::EndPopup();
		}
	};
	this->backgroundGui_ = guiTask;
	ImGui::OpenPopup("Load Volume");
	auto loaderTask = [fileNameStr, fileNamePath, progress, this](BackgroundWorker* worker)
	{
		//callbacks
		renderer::VolumeProgressCallback_t progressCallback = [progress](float v)
		{
			*progress.get() = v * 0.99f;
		};
		renderer::VolumeLoggingCallback_t logging = [](const std::string& msg)
		{
			std::cout << msg << std::endl;
		};
		int errorCode = 1;
		renderer::VolumeErrorCallback_t error = [&errorCode](const std::string& msg, int code)
		{
			errorCode = code;
			std::cerr << msg << std::endl;
		};
		//load it locally
		std::unique_ptr<renderer::Volume> volume;
		if (fileNamePath.extension() == ".dat")
			volume.reset(renderer::loadVolumeFromRaw(fileNameStr, progressCallback, logging, error));
		else if (fileNamePath.extension() == ".xyz")
			volume.reset(renderer::loadVolumeFromXYZ(fileNameStr, progressCallback, logging, error));
		else if (fileNamePath.extension() == ".cvol")
			volume = std::make_unique<renderer::Volume>(fileNameStr, progressCallback, logging, error);
		else {
			std::cerr << "Unrecognized extension: " << fileNamePath.extension() << std::endl;
		}
		if (volume != nullptr) {
			volume->getLevel(0)->copyCpuToGpu();
			std::swap(volume_, volume);
			volumeMipmapLevel_ = 0;
			volumeFilename_ = fileNamePath.filename().string();
			std::cout << "Loaded" << std::endl;

			volumeHistogram_ = volume_->extractHistogram();

			minDensity_ = (minDensity_ < volumeHistogram_.maxDensity && minDensity_ > volumeHistogram_.minDensity) ? minDensity_ : volumeHistogram_.minDensity;
			maxDensity_ = (maxDensity_ < volumeHistogram_.maxDensity && maxDensity_ > volumeHistogram_.minDensity) ? maxDensity_ : volumeHistogram_.maxDensity;
		}

		//set it in the GUI and close popup
		this->backgroundGui_ = {};
		ImGui::CloseCurrentPopup();
		triggerRedraw(RedrawRenderer);
	};
	//start background task
	worker_.launch(loaderTask);
}

static void HelpMarker(const char* desc)
{
	//ImGui::TextDisabled(ICON_FA_QUESTION);
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(desc);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}
void Visualizer::specifyUI()
{
	uiMenuBar();

	ImGui::PushItemWidth(ImGui::GetFontSize() * -8);

	uiVolume();
	uiComputationMode();
	uiCamera();
	if (renderMode_ == DirectVolumeRendering) {
		uiTfEditor();
	}
	uiRenderer();
	uiShading();

	ImGui::PopItemWidth();

	if (backgroundGui_)
		backgroundGui_();

	uiScreenshotOverlay();
	uiFPSOverlay();
}

void Visualizer::uiMenuBar()
{
	ImGui::BeginMenuBar();
	ImGui::Text("Hotkeys");
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted("'P': Screenshot");
		ImGui::TextUnformatted("'L': Lock foveated center");
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
	if (ImGui::SmallButton("Save##Settings"))
		settingsSave();
	if (ImGui::SmallButton("Load##Settings"))
		settingsLoad();
	ImGui::EndMenuBar();
	//hotkeys
	if (ImGui::IsKeyPressed(GLFW_KEY_P, false))
	{
		screenshot();
	}
}

void Visualizer::uiVolume()
{
	if (ImGui::CollapsingHeader("Volume", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::InputText("", &volumeFilename_[0], volumeFilename_.size() + 1, ImGuiInputTextFlags_ReadOnly);
		ImGui::SameLine();
		if (ImGui::Button(ICON_FA_FOLDER_OPEN "##Volume"))
		{
			loadVolume();
		}

		//functor for selecting the mipmap level, possible in a separate thread
		auto selectMipampLevel = [this](int level, renderer::Volume::MipmapFilterMode filter)
		{
			if (volume_ == nullptr) return;
			if (level == volumeMipmapLevel_ &&
				filter == volumeMipmapFilterMode_) return;
			if (volume_->getLevel(level) == nullptr || filter != volumeMipmapFilterMode_)
			{
				//resample in background thread
				worker_.wait(); //wait for current task
				auto guiTask = []()
				{
					if (ImGui::BeginPopupModal("Resample", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
					{
						const ImU32 col = ImGui::GetColorU32(ImGuiCol_ButtonHovered);
						ImGuiExt::Spinner("ResampleVolume", 50, 10, col);
						ImGui::EndPopup();
					}
				};
				this->backgroundGui_ = guiTask;
				ImGui::OpenPopup("Resample");
				auto resampleTask = [level, filter, this](BackgroundWorker* worker)
				{
					if (filter != volumeMipmapFilterMode_)
						volume_->deleteAllMipmapLevels();
					volume_->createMipmapLevel(level, filter);
					volume_->getLevel(level)->copyCpuToGpu();
					volumeMipmapLevel_ = level;
					volumeMipmapFilterMode_ = filter;
					//close popup
					this->backgroundGui_ = {};
					ImGui::CloseCurrentPopup();
					triggerRedraw(RedrawRenderer);
				};
				//start background task
				worker_.launch(resampleTask);
			}
			else
			{
				//just ensure, it is on the GPU
				volume_->getLevel(level)->copyCpuToGpu();
				volumeMipmapLevel_ = level;
				volumeMipmapFilterMode_ = filter; //not necessarily needed
				triggerRedraw(RedrawRenderer);
			}
		};
		//Level buttons
		for (int i = 0; i < sizeof(MipmapLevels) / sizeof(int); ++i)
		{
			int l = MipmapLevels[i];
			if (i > 0) ImGui::SameLine();
			std::string label = std::to_string(l + 1) + "x";
			if (ImGui::RadioButton(label.c_str(), volumeMipmapLevel_ == l))
				selectMipampLevel(l, volumeMipmapFilterMode_);
		}
		//Filter buttons
		ImGui::TextUnformatted("Filtering:");
		ImGui::SameLine();
		if (ImGui::RadioButton("Average",
			volumeMipmapFilterMode_ == renderer::Volume::MipmapFilterMode::AVERAGE))
			selectMipampLevel(volumeMipmapLevel_, renderer::Volume::MipmapFilterMode::AVERAGE);
		ImGui::SameLine();
		if (ImGui::RadioButton("Halton",
			volumeMipmapFilterMode_ == renderer::Volume::MipmapFilterMode::HALTON))
			selectMipampLevel(volumeMipmapLevel_, renderer::Volume::MipmapFilterMode::HALTON);

		//print statistics
		ImGui::Text("Resolution: %d, %d, %d\nSize: %5.3f, %5.3f, %5.3f",
			volume_ && volume_->getLevel(volumeMipmapLevel_) ? static_cast<int>(volume_->getLevel(volumeMipmapLevel_)->sizeX()) : 0,
			volume_ && volume_->getLevel(volumeMipmapLevel_) ? static_cast<int>(volume_->getLevel(volumeMipmapLevel_)->sizeY()) : 0,
			volume_ && volume_->getLevel(volumeMipmapLevel_) ? static_cast<int>(volume_->getLevel(volumeMipmapLevel_)->sizeZ()) : 0,
			volume_ ? volume_->worldSizeX() : 0,
			volume_ ? volume_->worldSizeY() : 0,
			volume_ ? volume_->worldSizeZ() : 0);

		if (volume_)
		{
			ImGui::Text("Min Density: %f\nMax Density: %f", volumeHistogram_.minDensity, volumeHistogram_.maxDensity);
		}
	}
}

void Visualizer::uiCamera()
{
	if (ImGui::CollapsingHeader("Camera")) {
		if (cameraGui_.specifyUI()) triggerRedraw(RedrawRenderer);
	}
	if (cameraGui_.updateMouse())
		triggerRedraw(RedrawRenderer);
}

void Visualizer::uiRenderer()
{
	if (ImGui::CollapsingHeader("Render Parameters")) {
		if (renderMode_ == IsosurfaceRendering)
		{
			double isoMin = 0.01, isoMax = 2.0;
			if (ImGui::SliderScalar("Isovalue", ImGuiDataType_Double, &rendererArgs_.isovalue, &isoMin, &isoMax, "%.5f", 2)) triggerRedraw(RedrawRenderer);
		}
		double stepMin = 0.01, stepMax = 1.0;
		if (ImGui::SliderScalar("Stepsize", ImGuiDataType_Double, &rendererArgs_.stepsize, &stepMin, &stepMax, "%.5f", 2)) triggerRedraw(RedrawRenderer);
		static const char* VolumeFilterModeNames[] = { "Trilinear", "Tricubic" };
		const char* currentFilterModeName = (rendererArgs_.volumeFilterMode >= 0 && rendererArgs_.volumeFilterMode < renderer::RendererArgs::_VOLUME_FILTER_MODE_COUNT_)
			? VolumeFilterModeNames[rendererArgs_.volumeFilterMode] : "Unknown";
		if (ImGui::SliderInt("Filter Mode", reinterpret_cast<int*>(&rendererArgs_.volumeFilterMode),
			0, renderer::RendererArgs::_VOLUME_FILTER_MODE_COUNT_ - 1, currentFilterModeName))
			triggerRedraw(RedrawRenderer);
		if (renderMode_ == IsosurfaceRendering)
		{
			int binaryMin = 0, binaryMax = 10;
			if (ImGui::SliderScalar("Binary Search", ImGuiDataType_S32, &rendererArgs_.binarySearchSteps, &binaryMin, &binaryMax, "%d")) triggerRedraw(RedrawRenderer);
			int aoSamplesMin = 0, aoSamplesMax = 512;
			if (ImGui::SliderScalar("AO Samples", ImGuiDataType_S32, &rendererArgs_.aoSamples, &aoSamplesMin, &aoSamplesMax, "%d", 2)) triggerRedraw(RedrawRenderer);
			double aoRadiusMin = 0.01, aoRadiusMax = 0.5;
			if (ImGui::SliderScalar("AO Radius", ImGuiDataType_Double, &rendererArgs_.aoRadius, &aoRadiusMin, &aoRadiusMax, "%.5f", 2)) triggerRedraw(RedrawRenderer);
		}
	}
}

void Visualizer::uiComputationMode()
{
	if (ImGui::RadioButton("Iso-surface",
		reinterpret_cast<int*>(&renderMode_), IsosurfaceRendering))
		triggerRedraw(RedrawRenderer);
	ImGui::SameLine();
	if (ImGui::RadioButton("Dvr",
		reinterpret_cast<int*>(&renderMode_), DirectVolumeRendering))
		triggerRedraw(RedrawRenderer);
}

void Visualizer::uiTfEditor()
{
	if (ImGui::CollapsingHeader("TF Editor"))
	{
		if (ImGui::Button(ICON_FA_FOLDER_OPEN " Load TF"))
		{
			// open file dialog
			auto results = pfd::open_file(
				"Load transfer function",
				tfDirectory_,
				{ "Transfer Function", "*.tf" },
				false
			).result();
			if (results.empty())
				return;
			std::string fileNameStr = results[0];

			auto fileNamePath = std::filesystem::path(fileNameStr);
			std::cout << "TF is loaded from " << fileNamePath << std::endl;
			tfDirectory_ = fileNamePath.string();

			editor_.loadFromFile(fileNamePath.string(), minDensity_, maxDensity_);
			triggerRedraw(RedrawRenderer);
		}
		ImGui::SameLine();
		if (ImGui::Button(ICON_FA_SAVE " Save TF"))
		{
			// save file dialog
			auto fileNameStr = pfd::save_file(
				"Save transfer function",
				tfDirectory_,
				{ "Transfer Function", "*.tf" },
				true
			).result();
			if (fileNameStr.empty())
				return;

			auto fileNamePath = std::filesystem::path(fileNameStr);
			fileNamePath = fileNamePath.replace_extension(".tf");
			std::cout << "TF is saved under " << fileNamePath << std::endl;
			tfDirectory_ = fileNamePath.string();

			editor_.saveToFile(fileNamePath.string(), minDensity_, maxDensity_);
		}
		ImGui::SameLine();
		ImGui::Checkbox("Show CPs", &showColorControlPoints_);

		ImGuiWindow* window = ImGui::GetCurrentWindow();
		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;

		//Color
		const ImGuiID tfEditorColorId = window->GetID("TF Editor Color");
		auto pos = window->DC.CursorPos;
		auto tfEditorColorWidth = window->WorkRect.Max.x - window->WorkRect.Min.x;
		auto tfEditorColorHeight = 50.0f;
		const ImRect tfEditorColorRect(pos, ImVec2(pos.x + tfEditorColorWidth, pos.y + tfEditorColorHeight));
		ImGui::ItemSize(tfEditorColorRect, style.FramePadding.y);
		ImGui::ItemAdd(tfEditorColorRect, tfEditorColorId);

		//Opacity
		const ImGuiID tfEditorOpacityId = window->GetID("TF Editor Opacity");
		pos = window->DC.CursorPos;
		auto tfEditorOpacityWidth = window->WorkRect.Max.x - window->WorkRect.Min.x;
		auto tfEditorOpacityHeight = 100.0f;
		const ImRect tfEditorOpacityRect(pos, ImVec2(pos.x + tfEditorOpacityWidth, pos.y + tfEditorOpacityHeight));

		auto histogramRes = (volumeHistogram_.maxDensity - volumeHistogram_.minDensity) / volumeHistogram_.getNumOfBins();
		int histogramBeginOffset = (minDensity_ - volumeHistogram_.minDensity) / histogramRes;
		int histogramEndOffset = (volumeHistogram_.maxDensity - maxDensity_) / histogramRes;
		auto maxFractionVal = *std::max_element(std::begin(volumeHistogram_.bins) + histogramBeginOffset, std::end(volumeHistogram_.bins) - histogramEndOffset);
		ImGui::PlotHistogram("", volumeHistogram_.bins + histogramBeginOffset, volumeHistogram_.getNumOfBins() - histogramEndOffset - histogramBeginOffset,
			0, NULL, 0.0f, maxFractionVal, ImVec2(tfEditorOpacityWidth, tfEditorOpacityHeight));

		editor_.init(tfEditorOpacityRect, tfEditorColorRect, showColorControlPoints_);
		editor_.handleIO();
		editor_.render();

		if (ImGui::SliderFloat("Opacity Scaling", &opacityScaling_, 1.0f, 500.0f))
		{
			triggerRedraw(RedrawRenderer);
		}
		if (ImGui::SliderFloat("Min Density", &minDensity_, volumeHistogram_.minDensity, volumeHistogram_.maxDensity))
		{
			triggerRedraw(RedrawRenderer);
		}
		if (ImGui::SliderFloat("Max Density", &maxDensity_, volumeHistogram_.minDensity, volumeHistogram_.maxDensity))
		{
			triggerRedraw(RedrawRenderer);
		}
		if (ImGui::Checkbox("Use Shading", &dvrUseShading_))
		{
			triggerRedraw(RedrawRenderer);
		}

		if (editor_.getIsChanged())
		{
			triggerRedraw(RedrawRenderer);
		}
	}
}

void Visualizer::uiShading()
{
	if (ImGui::CollapsingHeader("Output - Shading", ImGuiTreeNodeFlags_DefaultOpen)) {
		auto redraw = renderMode_ == IsosurfaceRendering
			? RedrawPost
			: RedrawRenderer;

		ImGuiColorEditFlags colorFlags = ImGuiColorEditFlags_Float | ImGuiColorEditFlags_PickerHueWheel;
		if (ImGui::ColorEdit3("Material Color", &materialColor.x, colorFlags)) triggerRedraw(redraw);
		if (ImGui::ColorEdit3("Ambient Light", &ambientLightColor.x, colorFlags)) triggerRedraw(redraw);
		if (ImGui::ColorEdit3("Diffuse Light", &diffuseLightColor.x, colorFlags)) triggerRedraw(redraw);
		if (ImGui::ColorEdit3("Specular Light", &specularLightColor.x, colorFlags)) triggerRedraw(redraw);
		float minSpecular = 0, maxSpecular = 64;
		if (ImGui::SliderScalar("Spec. Exp.", ImGuiDataType_Float, &specularExponent, &minSpecular, &maxSpecular, "%.3f", 2)) triggerRedraw(redraw);
		float minAO = 0, maxAO = 1;
		if (ImGui::SliderScalar("AO Strength", ImGuiDataType_Float, &aoStrength, &minAO, &maxAO)) triggerRedraw(RedrawPost);
		if (ImGuiExt::DirectionPicker2D("Light direction", &lightDirectionScreen.x, ImGuiExtDirectionPickerFlags_InvertXY))
			triggerRedraw(redraw);
		const char* currentChannelName = (channelMode_ >= 0 && channelMode_ < _ChannelCount_)
			? ChannelModeNames[channelMode_] : "Unknown";
		if (ImGui::SliderInt("Channel", reinterpret_cast<int*>(&channelMode_), 0, _ChannelCount_ - 1, currentChannelName))
			triggerRedraw(RedrawPost);
		if (channelMode_ == ChannelFlow)
			if (ImGui::Checkbox("Flow with Inpainting", &flowWithInpainting_))
				triggerRedraw(RedrawPost);
		if (ImGui::SliderInt("Temporal Smoothing", &temporalPostSmoothingPercentage_, 0, 100, "%d%%"))
			triggerRedraw(RedrawRenderer);
	}
}

void Visualizer::uiScreenshotOverlay()
{
	if (screenshotTimer_ <= 0) return;

	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x / 2, io.DisplaySize.y - 10);
	ImVec2 window_pos_pivot = ImVec2(0.5f, 1.0f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	//ImGui::PushStyleVar(ImGuiStyleVar_Alpha, alpha);
	ImGui::Begin("Example: Simple overlay", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav);
	ImGui::TextUnformatted(screenshotString_.c_str());
	ImGui::End();
	//ImGui::PopStyleVar(ImGuiStyleVar_Alpha);

	screenshotTimer_ -= io.DeltaTime;
}

void Visualizer::uiFPSOverlay()
{
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_pos = ImVec2(io.DisplaySize.x - 5, 5);
	ImVec2 window_pos_pivot = ImVec2(1.0f, 0.0f);
	ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
	ImGui::SetNextWindowBgAlpha(0.5f);
	ImGui::Begin("FPSDisplay", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav);
	ImGui::Text("FPS: %.1f", io.Framerate);
	std::string extraText = extraFrameInformation_.str();
	if (!extraText.empty())
	{
		extraText = extraText.substr(1); //strip initial '\n'
		ImGui::TextUnformatted(extraText.c_str());
	}
	extraFrameInformation_ = std::stringstream();
	ImGui::End();
}

renderer::RendererArgs Visualizer::setupRendererArgs(
	RenderMode renderMode, int upscaleFactor)
{
	cameraGui_.updateRenderArgs(rendererArgs_);
	renderer::RendererArgs args = rendererArgs_;
	args.cameraResolutionX = displayWidth_ / upscaleFactor;
	args.cameraResolutionY = displayHeight_ / upscaleFactor;
	args.cameraViewport = make_int4(0, 0, -1, -1);
	args.mipmapLevel = volumeMipmapLevel_;
	args.renderMode = (renderMode==IsosurfaceRendering)
		? renderer::RendererArgs::ISO : renderer::RendererArgs::DVR;
	args.densityAxisOpacity = editor_.getDensityAxisOpacity();
	args.opacityAxis = editor_.getOpacityAxis();
	args.densityAxisColor = editor_.getDensityAxisColor();
	args.colorAxis = editor_.getColorAxis();
	args.opacityScaling = opacityScaling_;
	args.minDensity = minDensity_;
	args.maxDensity = maxDensity_;

	renderer::ShadingSettings shading;
	shading.ambientLightColor = ambientLightColor;
	shading.diffuseLightColor = diffuseLightColor;
	shading.specularLightColor = specularLightColor;
	shading.specularExponent = specularExponent;
	shading.materialColor = materialColor;
	shading.aoStrength = aoStrength;
	shading.lightDirection = normalize(cameraGui_.screenToWorld(lightDirectionScreen));
	args.shading = shading;
	args.dvrUseShading = dvrUseShading_;

	return args;
}

void Visualizer::render(int display_w, int display_h)
{
	resize(display_w, display_h);

	if (volume_ == nullptr) return;
	if (volume_->getLevel(volumeMipmapLevel_) == nullptr) return;

	if (redrawMode_ == RedrawNone)
	{
		//just draw the precomputed texture
		drawer_.drawQuad(screenTextureGL_);
		return;
	}

	renderImpl(renderMode_);
}

void Visualizer::renderImpl(RenderMode renderMode)
{
	//render iso-surface to rendererOutput_
	if (redrawMode_ == RedrawRenderer)
	{
		int upscale_factor = 1;
		renderer::RendererArgs args = setupRendererArgs(renderMode, upscale_factor);
		args.aoSamples = renderMode==IsosurfaceRendering ? rendererArgs_.aoSamples : 0;

		int outputChannels = renderMode == IsosurfaceRendering
			? RENDERER_NAMESPACE::IsoRendererOutputChannels
			: RENDERER_NAMESPACE::DvrRendererOutputChannels;
		rendererOutput_ = RENDERER_NAMESPACE::OutputTensor(
			args.cameraResolutionY, args.cameraResolutionX, outputChannels);

		cudaStream_t stream = cuMat::Context::current().stream();
		render_gpu(volume_.get(), &args, rendererOutput_, stream);
		interpolatedFlowAvailable_ = false;

		redrawMode_ = RedrawPost;
	}

	//flow inpainting
	bool needFlow =
		temporalPostSmoothingPercentage_ > 0 ||
		(channelMode_ == ChannelFlow && flowWithInpainting_);
	if (needFlow && !interpolatedFlowAvailable_)
	{
		if (renderMode == IsosurfaceRendering)
		{
			interpolatedFlow_ = kernel::inpaintFlow(rendererOutput_, 0, 6, 7);
		}
		else //DVR
		{
			interpolatedFlow_ = kernel::inpaintFlow(rendererOutput_, 3, 8, 9);
		}
	}

	//select channel and write to screen texture
	//this also includes the temporal reprojection
	if (redrawMode_ == RedrawPost)
	{
		if (channelMode_ == ChannelFlow)
		{
			if (flowWithInpainting_)
			{
				kernel::selectOutputChannel(interpolatedFlow_, postOutput_,
					0, 1, -1, -2,
					10, 0.5, 1, 0);
			}
			else
			{
				kernel::selectOutputChannel(rendererOutput_, postOutput_,
					6, 7, -1, -2,
					10, 0.5, 1, 0);
			}
		}
		else {
			//temporal reprojection
			RENDERER_NAMESPACE::OutputTensor blendingOutput;
			if (previousBlendingOutput_.rows() != rendererOutput_.rows() ||
				previousBlendingOutput_.cols() != rendererOutput_.cols() ||
				previousBlendingOutput_.batches() != rendererOutput_.batches() ||
				temporalPostSmoothingPercentage_ == 0)
			{
				blendingOutput = rendererOutput_;
			}
			else
			{
				auto previousOutput = kernel::warp(
					previousBlendingOutput_,
					interpolatedFlow_);
				float blendingFactor = temporalPostSmoothingPercentage_ / 100.0f;
				blendingOutput = kernel::lerp(rendererOutput_, previousOutput, blendingFactor);
			}
			//channel selection
			if (renderMode == IsosurfaceRendering)
				selectChannelIso(channelMode_, blendingOutput, postOutput_);
			else
				selectChannelDvr(channelMode_, blendingOutput, postOutput_);
			previousBlendingOutput_ = blendingOutput;
		}

		redrawMode_ = RedrawNone;
	}

	cudaMemcpy(screenTextureCudaBuffer_, postOutput_, 4 * displayWidth_*displayHeight_,
		cudaMemcpyDeviceToDevice);
	copyBufferToOpenGL();

	//draw texture
	drawer_.drawQuad(screenTextureGL_);
}

void Visualizer::copyBufferToOpenGL()
{
	CUMAT_SAFE_CALL(cudaGraphicsMapResources(1, &screenTextureCuda_, 0));
	cudaArray* texture_ptr;
	CUMAT_SAFE_CALL(cudaGraphicsSubResourceGetMappedArray(&texture_ptr, screenTextureCuda_, 0, 0));
	size_t size_tex_data = sizeof(GLubyte) * displayWidth_ * displayHeight_ * 4;
	CUMAT_SAFE_CALL(cudaMemcpyToArray(texture_ptr, 0, 0, screenTextureCudaBuffer_, size_tex_data, cudaMemcpyDeviceToDevice));
	CUMAT_SAFE_CALL(cudaGraphicsUnmapResources(1, &screenTextureCuda_, 0));
}

void Visualizer::resize(int display_w, int display_h)
{
	//make it a nice multiplication of everything
	const int multiply = 4 * 3;
	display_w = display_w / multiply * multiply;
	display_h = display_h / multiply * multiply;

	if (display_w == displayWidth_ && display_h == displayHeight_)
		return;
	if (display_w == 0 || display_h == 0)
		return;
	releaseResources();
	displayWidth_ = display_w;
	displayHeight_ = display_h;

	//create texture
	glGenTextures(1, &screenTextureGL_);
	glBindTexture(GL_TEXTURE_2D, screenTextureGL_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // clamp s coordinate
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // clamp t coordinate
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RGBA8,
		displayWidth_, displayHeight_, 0
		, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

	//register with cuda
	CUMAT_SAFE_CALL(cudaGraphicsGLRegisterImage(
		&screenTextureCuda_, screenTextureGL_,
		GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard));

	//create channel output buffer
	CUMAT_SAFE_CALL(cudaMalloc(&screenTextureCudaBuffer_, displayWidth_ * displayHeight_ * 4 * sizeof(GLubyte)));
	CUMAT_SAFE_CALL(cudaMalloc(&postOutput_, displayWidth_ * displayHeight_ * 4 * sizeof(GLubyte)));

	glBindTexture(GL_TEXTURE_2D, 0);

	triggerRedraw(RedrawRenderer);
	std::cout << "Visualizer::resize(): " << displayWidth_ << ", " << displayHeight_ << std::endl;
}

void Visualizer::triggerRedraw(RedrawMode mode)
{
	redrawMode_ = std::max(redrawMode_, mode);
}

void Visualizer::selectChannelIso(ChannelMode mode, const RENDERER_NAMESPACE::OutputTensor& output, GLubyte* cudaBuffer) const
{
	if (output.rows() != displayHeight_ || output.cols() != displayWidth_)
	{
		std::cout << "Tensor shape does not match: expected=(1 * Channels * "
			<< displayHeight_ << " * " << displayWidth_ << "), but got: "
			<< output.rows() << " * " << output.cols() << std::endl;
		throw std::exception("Tensor shape does not match screen size");
	}
	switch (mode)
	{
	case ChannelMask:
		kernel::selectOutputChannel(output, cudaBuffer,
			0, 0, 0, 0,
			1, 0, 0, 1);
		break;
	case ChannelNormal:
		kernel::selectOutputChannel(output, cudaBuffer,
			1, 2, 3, 0,
			0.5, 0.5, 1, 0);
		break;
	case ChannelDepth: {
		auto minMaxDepth = kernel::extractMinMaxDepth(output, 4);
		float minDepth = minMaxDepth.first;
		float maxDepth = minMaxDepth.second;
		//std::cout << "depth: min=" << minDepth << ", max=" << maxDepth << std::endl;
		kernel::selectOutputChannel(output, cudaBuffer,
			4, 4, 4, 0,
			1 / (maxDepth - minDepth), -minDepth / (maxDepth - minDepth), 1, 0);
		break;
	}
	case ChannelAO:
		kernel::selectOutputChannel(output, cudaBuffer,
			5, 5, 5, 0,
			1, 0, 1, 0);
		break;
	case ChannelFlow:
		kernel::selectOutputChannel(output, cudaBuffer,
			6, 7, -1, -2,
			10, 0.5, 1, 0);
		break;
	case ChannelColor: {
		renderer::ShadingSettings settings;
		settings.ambientLightColor = ambientLightColor;
		settings.diffuseLightColor = diffuseLightColor;
		settings.specularLightColor = specularLightColor;
		settings.specularExponent = specularExponent;
		settings.materialColor = materialColor;
		settings.aoStrength = aoStrength;
		settings.lightDirection = lightDirectionScreen;
		kernel::screenShading(output, cudaBuffer, settings);
		break;
	}
	default:
		throw std::exception("unknown enum");
	}
}

void Visualizer::selectChannelDvr(ChannelMode mode, const RENDERER_NAMESPACE::OutputTensor& output, GLubyte* cudaBuffer) const
{
	if (output.rows() != displayHeight_ || output.cols() != displayWidth_)
	{
		std::cout << "Tensor shape does not match: expected=(1 * Channels * "
			<< displayHeight_ << " * " << displayWidth_ << "), but got: "
			<< output.rows() << " * " << output.cols() << std::endl;
		throw std::exception("Tensor shape does not match screen size");
	}
	switch (mode)
	{
	case ChannelColor:
		kernel::selectOutputChannel(output, cudaBuffer,
			0, 1, 2, 3,
			1, 0, 0, 1); //color is pre-multiplied with alpha
		break;
	case ChannelMask:
		kernel::selectOutputChannel(output, cudaBuffer,
			3, 3, 3, 3,
			1, 0, 0, 1);
		break;
	case ChannelNormal:
		kernel::selectOutputChannel(output, cudaBuffer,
			4, 5, 6, 3,
			0.5, 0.5, 1, 0);
		break;
	case ChannelDepth: {
		auto minMaxDepth = kernel::extractMinMaxDepth(output, 7);
		float minDepth = minMaxDepth.first;
		float maxDepth = minMaxDepth.second;
		//std::cout << "depth: min=" << minDepth << ", max=" << maxDepth << std::endl;
		kernel::selectOutputChannel(output, cudaBuffer,
			7, 7, 7, 3,
			1 / (maxDepth - minDepth), -minDepth / (maxDepth - minDepth), 1, 0);
		break;
	}
	case ChannelAO:
		kernel::selectOutputChannel(output, cudaBuffer,
			-1, -1, -1, -1, //disabled
			1, 0, 1, 0);
		break;
	case ChannelFlow:
		kernel::selectOutputChannel(output, cudaBuffer,
			8, 9, -1, -2,
			10, 0.5, 1, 0);
		break;
	default:
		throw std::exception("unknown enum");
	}
}

void Visualizer::screenshot()
{
	std::string folder = "screenshots";

	char time_str[128];
	time_t now = time(0);
	struct tm tstruct;
	localtime_s(&tstruct, &now);
	strftime(time_str, sizeof(time_str), "%Y%m%d-%H%M%S", &tstruct);

	char output_name[512];
	sprintf(output_name, "%s/screenshot_%s_%s.png", folder.c_str(), time_str, ChannelModeNames[channelMode_]);

	std::cout << "Take screenshot: " << output_name << std::endl;
	std::filesystem::create_directory(folder);

	std::vector<GLubyte> textureCpu(4 * displayWidth_ * displayHeight_);
	CUMAT_SAFE_CALL(cudaMemcpy(&textureCpu[0], screenTextureCudaBuffer_, 4 * displayWidth_*displayHeight_, cudaMemcpyDeviceToHost));

	if (lodepng_encode32_file(output_name, textureCpu.data(), displayWidth_, displayHeight_) != 0)
	{
		std::cerr << "Unable to save image" << std::endl;
		screenshotString_ = std::string("Unable to save screenshot to ") + output_name;
	}
	else
	{
		screenshotString_ = std::string("Screenshot saved to ") + output_name;
	}
	screenshotTimer_ = 2.0f;
}

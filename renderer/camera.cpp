#include "camera.h"

#include <iostream>
#include <iomanip>

#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/quaternion.hpp"
#include "glm/gtc/matrix_access.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace std
{
	std::ostream& operator<<(std::ostream& o, const glm::vec3 v)
	{
		o << std::fixed << std::setw(5) << std::setprecision(3)
			<< v.x << "," << v.y << "," << v.z;
		return o;
	}
	std::ostream& operator<<(std::ostream& o, const glm::vec4 v)
	{
		o << std::fixed << std::setw(5) << std::setprecision(3)
			<< v.x << "," << v.y << "," << v.z << "," << v.w;
		return o;
	}
	std::ostream& operator<<(std::ostream& o, const glm::mat4 m)
	{
		o << m[0] << "\n" << m[1] << "\n" << m[2] << "\n" << m[3];
		return o;
	}
}

BEGIN_RENDERER_NAMESPACE

namespace{
	// copy of glm::perspectiveFovLH_ZO, seems to not be defined in unix
	glm::mat4 perspectiveFovLH_ZO(float fov, float width, float height, float zNear, float zFar)
	{
		assert(width > static_cast<float>(0));
		assert(height > static_cast<float>(0));
		assert(fov > static_cast<float>(0));

		float const rad = fov;
		float const h = glm::cos(static_cast<float>(0.5) * rad) / glm::sin(static_cast<float>(0.5) * rad);
		float const w = h * height / width; ///todo max(width , Height) / min(width , Height)?

		glm::mat4 Result(static_cast<float>(0));
		Result[0][0] = w;
		Result[1][1] = h;
		Result[2][2] = zFar / (zFar - zNear);
		Result[2][3] = static_cast<float>(1);
		Result[3][2] = -(zFar * zNear) / (zFar - zNear);
		return Result;
	}
}

void Camera::computeMatrices(float3 cameraOrigin_, float3 cameraLookAt_, float3 cameraUp_, float fovDegrees,
	int width, int height, float nearClip, float farClip, float4 viewMatrixOut[4], float4 viewMatrixInverseOut[4],
	float4 normalMatrixOut[4])
{
	const glm::vec3 cameraOrigin = *reinterpret_cast<glm::vec3*>(&cameraOrigin_.x);
	const glm::vec3 cameraLookAt = *reinterpret_cast<glm::vec3*>(&cameraLookAt_.x);
	const glm::vec3 cameraUp = *reinterpret_cast<glm::vec3*>(&cameraUp_.x);

	float fovRadians = glm::radians(fovDegrees);

	glm::mat4 viewMatrix = glm::lookAtLH(cameraOrigin, cameraLookAt, normalize(cameraUp));
	glm::mat4 projMatrix = perspectiveFovLH_ZO(fovRadians, float(width), float(height), nearClip, farClip);

	glm::mat4 viewProjMatrix = projMatrix * viewMatrix;
	glm::mat4 invViewProjMatrix = glm::inverse(viewProjMatrix);
	glm::mat4 normalMatrix = glm::inverse(glm::transpose(glm::mat4(glm::mat3(viewMatrix))));

	viewProjMatrix = glm::transpose(viewProjMatrix);
	invViewProjMatrix = glm::transpose(invViewProjMatrix);
	normalMatrix = glm::transpose(normalMatrix);
	normalMatrix[0] = -normalMatrix[0]; //somehow, the networks were trained with normal-x inverted
	for (int i = 0; i < 4; ++i) viewMatrixOut[i] = *reinterpret_cast<float4*>(&viewProjMatrix[i].x);
	for (int i = 0; i < 4; ++i) viewMatrixInverseOut[i] = *reinterpret_cast<float4*>(&invViewProjMatrix[i].x);
	for (int i = 0; i < 4; ++i) normalMatrixOut[i] = *reinterpret_cast<float4*>(&normalMatrix[i].x);
}

END_RENDERER_NAMESPACE

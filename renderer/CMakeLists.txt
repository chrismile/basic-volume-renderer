cmake_minimum_required(VERSION 3.10)

# OUTPUT CACHE VARIABLES:
# BINDING_NAME
# LIBRARY_INCLUDE_DIR
# LIBRARY_LINK_LIBRARIES

option(RENDERER_SHARED_LIB "Build renderer as a shared library, needed only when used as Python extension" OFF)

# your configuraton
set(LIBRARY_HEADER_FILES
	commons.h
	lib.h
	errors.h
	volume.h
	renderer.h
	settings.h
	camera.h
	helper_math.h
	halton_sampler.h
	tf_texture_1d.h
	inpainting.h
	warping.h
	)
set(LIBRARY_CUDA_FILES
	renderer.cu
	tf_texture_1d.cu
	volume.cu
	inpainting.cu
	warping.cu
	)
set(LIBRARY_SOURCE_FILES
	volume.cpp
	settings.cpp
	camera.cpp
	tf_texture_1d.cpp
	python_bindings.cpp
	)
# the target name of the library
set(LIBRARY_NAME Renderer CACHE INTERNAL "the target name of the library, also used for the binding")

#add_library(${LIBRARY_NAME}_cuda OBJECT ${LIBRARY_CUDA_FILES})

# the library, compiled as a shared library
if(RENDERER_SHARED_LIB)
	add_library(${LIBRARY_NAME} SHARED
		${LIBRARY_HEADER_FILES} ${LIBRARY_SOURCE_FILES} ${LIBRARY_CUDA_FILES})
else(RENDERER_SHARED_LIB)
	add_library(${LIBRARY_NAME} STATIC
		${LIBRARY_HEADER_FILES} ${LIBRARY_SOURCE_FILES} ${LIBRARY_CUDA_FILES})
endif(RENDERER_SHARED_LIB)

set_target_properties(${LIBRARY_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)
target_compile_options(${LIBRARY_NAME} PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:${MY_CUDA_NVCC_FLAGS}>)
set_property(TARGET ${LIBRARY_NAME} PROPERTY CUDA_STANDARD 17)
# get include path to the first library header, to be used in the test application
list(GET ${LIBRARY_HEADER_FILES} 0 LIBRARY_INCLUDE_DIRb)
get_filename_component(LIBRARY_INCLUDE_DIRa ${LIBRARY_INCLUDE_DIRb} ABSOLUTE)
get_filename_component(LIBRARY_INCLUDE_DIRc ${LIBRARY_INCLUDE_DIRa} DIRECTORY)
set(LIBRARY_INCLUDE_DIR ${LIBRARY_INCLUDE_DIRc} CACHE FILEPATH "include directory of the custom library") 
target_include_directories(${LIBRARY_NAME}
	PRIVATE 
		${PYTHON_INCLUDE_DIR} 
		${CMAKE_SOURCE_DIR}/third-party/cuMat 
		${GLM_INCLUDE_DIRS}
		${GLEW_INCLUDE_DIR} 
		${GLFW_INCLUDE_DIRS}
	INTERFACE ${LIBRARY_INCLUDE_DIR}
	)
# libraries, also used in the test application
set(LIBRARY_LINK_LIBRARIES
	${PYTHON_LIBRARY}
	CACHE STRING "python and torch libraries")
target_link_libraries(${LIBRARY_NAME}
	${LIBRARY_LINK_LIBRARIES}
	${OPENGL_LIBRARY} 
	${GLEW_LIBRARY_RELEASE} 
	${GLFW_LIBRARIES})
set_property(TARGET ${LIBRARY_NAME} PROPERTY CXX_STANDARD 17)

if(RENDERER_SHARED_LIB)
	target_compile_definitions(${LIBRARY_NAME}
		PUBLIC RENDERER_BUILD_SHARED 
		PRIVATE BUILD_MAIN_LIB)
else(RENDERER_SHARED_LIB)
	# no extra definitions
endif(RENDERER_SHARED_LIB)

if(RENDERER_SHARED_LIB)
# post-build commands, copies the dll to the bin/ folder
get_filename_component(LIBRARY_INCLUDE_DIR ${LIBRARY_INCLUDE_DIRa} DIRECTORY)
add_custom_command(TARGET ${LIBRARY_NAME}
	POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/bin
	COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${LIBRARY_NAME}> ${CMAKE_SOURCE_DIR}/bin/${LIBRARY_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}

	COMMENT "Copies the libarary .dll to bin/"
	WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/..
	
	VERBATIM
	)
endif(RENDERER_SHARED_LIB)

# debugging: keep .ptx files of the kernels
#set_property(TARGET ${LIBRARY_NAME}_cuda PROPERTY CUDA_PTX_COMPILATION ON)
#target_compile_options(${LIBRARY_NAME} PRIVATE "--keep")